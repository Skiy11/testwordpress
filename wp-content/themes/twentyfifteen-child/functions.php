<?php
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function resize_logo() {
    add_image_size('logo-size', 250, 210);
}
add_action( 'after_setup_theme', 'resize_logo' );

function register_filter_ajax_widget() {
    register_widget( 'WidgetFilterAjax' );
    register_widget( 'WidgetFilter' );
}
add_action( 'widgets_init', 'register_filter_ajax_widget' );

/*Add custom post type*/
require_once( __DIR__ . '/extentions/post-type-music.php');
/*Add custom meta-data*/
require_once( __DIR__ . '/extentions/custom-meta-data.php');
/*Add custom taxonomy*/
require_once( __DIR__ . '/extentions/custom-taxonomy.php');
/*Add options Theme*/
require_once( __DIR__ . '/extentions/option-theme.php');
/*Add terms taxonomy in menu*/
/*require_once( __DIR__ . '/extentions/custom-menu.php');*/
require_once( __DIR__ . '/extentions/custom-menu-walker.php');
/*Registering a sidebar*/
require_once( __DIR__ . '/extentions/registering-sidebar.php');
/*Create widget*/
require_once( __DIR__ . '/extentions/menu-term-filter.php');
/*Create Ajax widget*/
require_once( __DIR__ . '/extentions/ajax-filter.php');
