<?php
/**
 * Template Name: Music Page
 */

get_header(); ?>

<section id="primary" class="content-area">
    <header class="page-header">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </header>

    <?php
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

        $filterTerm = filter_var_array($_GET, FILTER_VALIDATE_INT);

        $taxQuery['relation'] = 'OR';

        foreach ($filterTerm as $keyTaxonomy => $taxonomy) {
            if ($taxonomy) {
                $termsId = [];
                foreach ($taxonomy as $term) {
                    $termsId[] = $term;
                }
                $taxQuery[] = array(
                    'taxonomy'  => $keyTaxonomy,
                    'field'     => 'term_id',
                    'terms'     => $termsId,
                );
            }
        }

        $args = array(
            'post_type'      => 'music',
            'tax_query'      => $taxQuery,
            'posts_per_page' => 10,
            /*'paged'          => $paged,*/
        );

        include_once( get_stylesheet_directory(). '/content-page-music.php');
    ?>

</section><!-- .content-area -->
<?php get_footer(); ?>
