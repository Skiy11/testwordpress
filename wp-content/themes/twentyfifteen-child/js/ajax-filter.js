jQuery(document).ready(function($) {
    jQuery(".get-submit").css("display", "none");

    /*var dataSelected = history.state;*/
    /*var urlParameters = location.search;
    var authorTemp = [];
    var genreTemp = [];

    if (urlParameters != '' && history.state == null) {
        var dataMusic = {};
        var request = urlParameters.substring(urlParameters.indexOf('?') + 1).split('&');
        for (var i = 0; i < request.length; i++) {
            if(!request[i])
                continue;
            var pair = request[i].split('=');
            /!*pair[0] = pair[0].slice(0, -2);*!/
            pair[0] = pair[0].replace(/[^a-z]/g, '');

            if (pair[0] == 'authorm') {
                authorTemp.push(decodeURIComponent(pair[1]));
                dataMusic[pair[0]] = authorTemp;
            } else {
                genreTemp.push(decodeURIComponent(pair[1]));
                dataMusic[pair[0]] = genreTemp;
            }
        }
        console.log(dataMusic);
        var pushHistory = 'pushState';
        ajaxSender(dataMusic, pushHistory);
    }*/

    jQuery(".textwidget").on('click', "input[type='checkbox']", function() {
        var selected_author = [];
        var selected_genre = [];
        jQuery('input:checked').each(function () {
            if(jQuery(this).attr('data-taxonomy')=='authorm') {
                selected_author.push(jQuery(this).val());
            } else if(jQuery(this).attr('data-taxonomy')=='genre') {
                selected_genre.push(jQuery(this).val());
            }
        });

        var dataMusic = {
            authorm: selected_author,
            genre: selected_genre
        };

        var pushHistory = 'pushState';
        ajaxSender(dataMusic, pushHistory);
    });

    window.addEventListener('popstate', function (e) {
        var dataMusic = {};
        var dataSelected = history.state;

        if (dataSelected != null) {
            dataMusic = {
                authorm: dataSelected.authorm,
                genre: dataSelected.genre
            };
        } else {
            /*dataMusic = urlParameters.substring(urlParameters.indexOf('?') + 1).split('&');*/
            var urlParameters = location.search;
            var authorTemp = [];
            var genreTemp = [];

            if (urlParameters != '') {
                var request = urlParameters.substring(urlParameters.indexOf('?') + 1).split('&');
                for (var i = 0; i < request.length; i++) {
                    if (!request[i])
                        continue;
                    var pair = request[i].split('=');
                    /*pair[0] = pair[0].slice(0, -2);*/
                    pair[0] = pair[0].replace(/[^a-z]/g, '');

                    if (pair[0] == 'authorm') {
                        authorTemp.push(decodeURIComponent(pair[1]));
                        dataMusic[pair[0]] = authorTemp;
                    } else {
                        genreTemp.push(decodeURIComponent(pair[1]));
                        dataMusic[pair[0]] = genreTemp;
                    }
                }
            }
        }

        var arrayUrl = location.href.split('/'),
            paged = 1;


        if( typeof (arrayUrl[5]) != 'undefined'  ){
            paged = arrayUrl[5];
        }

        console.log(dataMusic);

        var pushHistory = null;

        ajaxSender(dataMusic, pushHistory, paged);

        var checkbox = jQuery('.textwidget input:checkbox');
        console.log(checkbox);
        if (dataMusic != null) {
            checkbox.prop('checked', false);
            checkbox.each(function () {
                if ((jQuery.inArray(jQuery(this).val(), dataMusic.authorm)) !== -1 || (jQuery.inArray(jQuery(this).val(), dataMusic.genre)) !== -1) {
                    jQuery(".textwidget input:checkbox[value='" + jQuery(this).val() + "']").prop('checked', true);
                }
            });
        } else {
            checkbox.prop('checked', false);
        }
    });

    function ajaxSender(dataMusic, pushHistory, page) {
        var data = {
            action: 'my_action',
            nonce: myajax.nonce
        };
        data.pageNumber = page;
        data.music = dataMusic;
        /*console.log(data);*/
        jQuery.post(myajax.url, data, function (response) {
            if (response.success) {
                jQuery("#main").replaceWith(response.data.htmlTemplate);
                if (pushHistory != null) {
                    var state = {
                        "authorm": data.music.authorm,
                        "genre": data.music.genre
                    };

                    history.pushState(state, null, response.data.getUrl);
                }
            }
        });
    }

/*    function URLToArray(url) {
        var request = {};
        var pairs = url.substring(url.indexOf('?') + 1).split('&');
        for (var i = 0; i < pairs.length; i++) {
            if(!pairs[i])
                continue;
            var pair = pairs[i].split('=');
            request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
        }
        return request;
    }*/

});
