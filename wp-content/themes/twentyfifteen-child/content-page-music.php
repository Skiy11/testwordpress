<main id="main" class="site-main" role="main">

    <?php

    if( empty($args['paged']) ){
        $args['paged'] = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    }

    $musicPost = new WP_Query($args);
    if ($musicPost->have_posts()) {
        while ($musicPost->have_posts()) :
            $musicPost->the_post();
            get_template_part( 'content-music', get_post_format() );
        endwhile;
    } else {
        get_template_part( 'content', 'none' );
    }

    $big = 999999999;

    ?>

    <div class="entry-content entry-content">
        <?php
        echo paginate_links( array(
            'base'     => get_page_link( get_option('page_music') ) . 'page/%#%',
            'format'   => '?paged=%#%',
            'current'  => max( 1, $musicPost->query_vars['paged'] ),
            'total'    => $musicPost->max_num_pages,
            'add_args' => array(
                          'authorm' => $music['authorm'],
                          'genre'   => $music['genre'],
            )
        ) );
        ?>
    </div>

</main><!-- .site-main -->
