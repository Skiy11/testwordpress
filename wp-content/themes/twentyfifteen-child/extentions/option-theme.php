<?php
function theme_settings_page()
{
    ?>
    <div class="wrap">
        <h1>Мои настройки темы</h1>
        <form method="post" enctype="multipart/form-data" action="options.php">
            <?php
            settings_fields("my-custom-field");
            do_settings_sections("my-theme-options");
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

function add_theme_menu_item()
{
    add_menu_page("Theme Panel",
                  "Настройки темы",
                  "manage_options",
                  "my-theme-panel",
                  "theme_settings_page",
                  null,
                  61
    );
}
add_action("admin_menu", "add_theme_menu_item");

/*Add logo*/

function logo_display()
{
    $logo = get_option('logo');
    if ($logo && $logo != '') {
        echo wp_get_attachment_image($logo, 'logo-size');
        ?>
        <br>
        <input type="file" name="logo"/>
        <p>
            <input type="checkbox" name="delete-logo"/>
            <label for = "delete-logo"> Удалить </label>
        </p>
        <?php
    } else {
        echo "<p>Лого не найден</p>";
        ?>
        <br>
        <input type="file" name="logo"/>
        <p>
            <input type="checkbox" name="delete-logo" checked/>
            <label for="delete-logo"> Удалить </label>
        </p>
        <?php
    }
}

function handle_logo_upload()
{
    if ($_FILES['logo']['tmp_name']) {
        $mediaHandle = media_handle_upload('logo', 0, [], ["test_form" => FALSE]);
        if (is_int($mediaHandle)) {
            return $mediaHandle;
        }
    } else {
        if($_POST['delete-logo']) {
            return '';
        }
        return get_option('logo');
    }
}

/*Add page list*/

function display_page_music_element()
{
    $args = array(
        'sort_order'   => 'asc',
        'sort_column'  => 'post_title',
        'hierarchical' => 1,
        'exclude'      => '',
        'include'      => '',
        'meta_key'     => '_wp_page_template',
        'meta_value'   => 'page-music.php',
        'authors'      => '',
        'child_of'     => 0,
        'parent'       => -1,
        'exclude_tree' => '',
        'number'       => '',
        'offset'       => 0,
        'post_type'    => 'page',
        'post_status'  => 'publish'
    );

    $allPages = get_pages( $args ); ?>

    <?php
    if (!empty($allPages)) : ?>
        <select name="page_music" id="page_music">
            <?php foreach($allPages as $page) : ?>
                <option value="<?=$page->ID; ?>" <?php selected(get_option('page_music'),$page->ID); ?>>
                    <?php echo $page->post_title; ?>
                </option>
            <?php endforeach; ?>
        </select>
    <?php
    else :
        echo 'Не найдено страницы с необходимым шаблоном. 
              Укажите одной из страниц шаблон - Music Page, 
              иначе часть функционала будет утеряна.';
    endif;

    $pageSlug = get_page_template_slug( get_option('page_music') );

    echo $pageSlug;
}

/*Add copyright*/

function display_copyright_element()
{
    ?>
    <input type="text" name="copyright" id="copyright" value="<?php echo get_option("copyright"); ?>" />
    <?php
}

/*Display logo and copyright in Theme Panel settings*/

function display_theme_panel_fields()
{
    add_settings_section("my-custom-field", "Настройки", null, "my-theme-options");
    add_settings_field("copyright",
                       "Введите copyright",
                       "display_copyright_element",
                       "my-theme-options",
                       "my-custom-field"
    );
    add_settings_field("logo",
                       "Логотип",
                       "logo_display",
                       "my-theme-options",
                       "my-custom-field"
    );
    add_settings_field("page_music",
                       "Список страниц",
                       "display_page_music_element",
                       "my-theme-options",
                       "my-custom-field"
    );
    register_setting("my-custom-field", "page_music");
    register_setting("my-custom-field", "copyright");
    register_setting("my-custom-field", "logo", "handle_logo_upload");
}
add_action("admin_init", "display_theme_panel_fields");