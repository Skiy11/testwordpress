<?php
function music_post_type()
{
    register_post_type('music',
        [
            'labels'      => [
                'name'          => __('Музыка'),
                'singular_name' => __('Музыка'),
                'menu_name'     => __('Музыка', 'twentyfifteen-child' ),
                'name_admin_bar'=> __('Музыка', 'twentyfifteen-child' ),
            ],
            'public'          => true,
            'has_archive'     => true,
            'rewrite'         => ['slug' => 'music'],
            'show_ui'         => true,
            'show_in_menu'    => true,
            'taxonomies'      => ['authorm', 'genre'],
            'capability_type' => 'post',
            'supports'        => ['title', 'editor', 'author', 'thumbnail'],
            'menu_position'   => 5,
        ]
    );
}
add_action('init', 'music_post_type');
