<?php

class HierarchicalMenu extends Walker_Nav_Menu
{
    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0)
    {
        $args = array();
        $taxonomies = get_taxonomies($args, $output);

        $output .= "<li id=\"menu-item\" class=\"menu-item menu-item-has-children\"><a href=" . $item->url . ">" . $item->title . "</a>";
        $output .= "<ul class='sub-menu toggled-on'>";

        if($taxonomies) {
            foreach ($taxonomies as $my_taxonomy) {
                if ($my_taxonomy->name == 'authorm' || $my_taxonomy->name == 'genre') {
                    $output .= "<li class='menu-item  menu-item-has-children'><a href='#'>$my_taxonomy->label</a>";
                    $terms = get_terms(array('taxonomy' => array($my_taxonomy->name)));
                    $output .= "<ul class='sub-menu toggled-on'>";
                    foreach ($terms as $term) {
                        $output .= "<li><a href=\"". get_term_link($term, $taxonomy = '') ."\">$term->name</a></li>";
                    }
                    $output .= "</ul>";
                    $output .= "</li>";
                }
            }
        }

        $output .= "</ul>";
        $output .= "</li>";
    }
}
