<?php
class WidgetFilterAjax extends WP_Widget {

    function __construct()
    {
        parent::__construct(
            'filtr-terms-ajax',
            'Аякс фильтр для термов'
        );
        if( wp_doing_ajax() ) {
            add_action('wp_ajax_my_action', array($this, 'my_action_callback'));
            add_action('wp_ajax_nopriv_my_action', array($this, 'my_action_callback'));
        }
        add_action('wp_enqueue_scripts', array($this, 'myajax_data'), 99);
    }

    public function myajax_data()
    {
        wp_localize_script('my_action_javascript', 'myajax',
            array(
                'url'          => admin_url('admin-ajax.php'),
                'nonce'        => wp_create_nonce('myajax-nonce'),
            )
        );

        wp_enqueue_script( 'my_action_javascript', get_stylesheet_directory_uri() . '/js/ajax-filter.js', ['jquery'], '1.0.0', true );
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }

        $taxonomies = $instance['taxonomies'];

        echo '<form class="textwidget" method="get" action="" autocomplete="off">';
        foreach ($taxonomies as $taxonomyKey => $taxonomy) {
            if (isset($taxonomy)) {
                $oneTaxonomy = get_taxonomies($args = array('name' => $taxonomyKey), $output = 'objects');

                echo '<h4>'. $oneTaxonomy[$taxonomyKey]->label . '</h4>';

                $terms = get_terms(array('taxonomy' => array($oneTaxonomy[$taxonomyKey]->name)));

                foreach ($terms as $term) {
                    $termData = $oneTaxonomy[$taxonomyKey]->name . '-' . $term->term_id;
                    $termDataTaxonomy = $oneTaxonomy[$taxonomyKey]->name;
                    $arrayTaxonomy = $oneTaxonomy[$taxonomyKey]->name;
                    if (isset($_GET[$termDataTaxonomy])) {
                        if (in_array($term->term_id, $_GET[$termDataTaxonomy])) {
                            echo $this->input_template($termData,
                                                       $arrayTaxonomy,
                                                       $term->term_id,
                                                       $term->name,
                                                       $checkedOption = 'checked');
                        } else {
                            echo $this->input_template($termData,
                                                       $arrayTaxonomy,
                                                       $term->term_id,
                                                       $term->name);
                        }
                    } else {
                        echo $this->input_template($termData,
                                                   $arrayTaxonomy,
                                                   $term->term_id,
                                                   $term->name);
                    }
                }
            }
        }

        echo '<input type="submit" class="get-submit" name="submit" value="Filter" />';
        echo '</form>';
        echo '</div>';
        echo $args['after_widget'];
    }

    public function my_action_callback()
    {
        $nonce = $_POST['nonce'];

        if ( /*empty($_POST['music']) || */! wp_verify_nonce( $nonce, 'myajax-nonce' ) ) {
            wp_send_json_error();
        }

        ob_start();

        $paged = isset($_POST['pageNumber']) ? absint( $_POST['pageNumber'] ) : 1;

        /*$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;*/

        $taxQuery['relation'] = 'OR';

        $partsUrlArray = array();

        if (isset($_POST['music'])) {
            $music = filter_var_array($_POST['music'], FILTER_VALIDATE_INT);
            foreach ($music as $keyMusic => $musicVal) {
                $taxQuery[] = array(
                    'taxonomy'  => $keyMusic,
                    'field'     => 'term_id',
                    'terms'     => $musicVal,
                );

                foreach ($musicVal as $musicUrlParams) {
                    $partsUrlArray[$keyMusic . "[" . $musicUrlParams . "]"] =  $musicUrlParams;
                }

            }
        }

        $args = array(
            'post_type'      => 'music',
            'tax_query'      => $taxQuery,
            'posts_per_page' => 10,
            'paged'          => $paged,
        );

        include_once( get_stylesheet_directory(). '/content-page-music.php');

        $return['htmlTemplate'] = ob_get_clean();

        $getUrl = get_page_link(get_option('page_music')) . '?';
        $getUrl .= http_build_query($partsUrlArray);
        $return['getUrl'] = preg_replace('/%5B[0-9]+%5D/simU', '[]', $getUrl);

        wp_send_json_success($return);
        //echo $htmlTemplate;

        wp_die();
    }

    public function input_template($id, $name, $value, $label, $checkedOption = '')
    {
        $inputMarkup = '<input class="checkbox-music" type="checkbox"' . $checkedOption . ' 
                            data-taxonomy="' . esc_attr__($name) . '" 
                            id="' . esc_attr__($id) . '"
                            name="' . esc_attr__($name) . "[]" . '"
                            value="' . esc_attr__($value) . '"/>
                            <label for="' . esc_attr__($id) . '">' . esc_html__($label) . '</label><br>';
        return $inputMarkup;
    }

    public function form( $instance )
    {

        $title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'text_domain');
        $taxonomies = get_object_taxonomies('music', $output = 'objects');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <?php esc_attr_e('Title:', 'text_domain'); ?>
            </label>
            <input class="widefat"
                   id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                   type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <?php foreach($taxonomies as $taxonomy) { ?>
                <input class="checkbox"
                       type="checkbox" <?php checked( $instance['taxonomies'][$taxonomy->name], 'on'); ?>
                       id="<?php echo esc_attr($this->get_field_id($taxonomy->name)); ?>"
                       name="<?php echo esc_attr($this->get_field_name($taxonomy->name)); ?>" />
                <label for="<?php echo esc_attr($this->get_field_id($taxonomy->name)); ?>">
                    <?php echo esc_html($taxonomy->label) ?>
                </label>
                <br>
            <?php } ?>
        </p>
        <?php

    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';

        $taxonomies = get_object_taxonomies('music', $output = 'objects');
        foreach($taxonomies as $taxonomy) {
            $instance['taxonomies'][$taxonomy->name] = $new_instance[$taxonomy->name];
        }

        return $instance;
    }

}

$my_widget = new WidgetFilterAjax();
