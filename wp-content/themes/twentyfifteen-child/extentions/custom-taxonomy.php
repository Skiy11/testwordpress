<?php
/*Add taxonomy author music*/
function register_taxonomy_author_music()
{
    $labels = [
        'name'              => _x('Автора', 'taxonomy general name'),
        'singular_name'     => _x('Автор', 'taxonomy singular name'),
        'search_items'      => __('Поиск по автору'),
        'all_items'         => __('Все автора'),
        'edit_item'         => __('Редактировать автора'),
        'update_item'       => __('Обновить автора'),
        'add_new_item'      => __('Добавить нового автора'),
        'new_item_name'     => __('Имя нового автора'),
        'menu_name'         => __('Автор'),
    ];
    $args = [
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'authorm'],
        'show_in_nav_menus' => true,
        'show_tagcloud'     => true,
    ];
    register_taxonomy('authorm', ['music'], $args);
}
add_action('init', 'register_taxonomy_author_music');

/*Add taxonomy genre*/
function register_taxonomy_genre()
{
    $labels = [
        'name'              => _x('Жанры', 'taxonomy general name'),
        'singular_name'     => _x('Жанр', 'taxonomy singular name'),
        'search_items'      => __('Поиск жанров'),
        'all_items'         => __('Все жанры'),
        'edit_item'         => __('Редактировать жанр'),
        'update_item'       => __('Обновить жанр'),
        'add_new_item'      => __('Добавить новый жанр'),
        'new_item_name'     => __('Название нового жанра'),
        'menu_name'         => __('Жанр'),
    ];
    $args = [
        'hierarchical'      => false,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'genre'],
        'show_in_nav_menus' => true,
        'show_tagcloud'     => true,
    ];
    register_taxonomy('genre', ['music'], $args);
}
add_action('init', 'register_taxonomy_genre');