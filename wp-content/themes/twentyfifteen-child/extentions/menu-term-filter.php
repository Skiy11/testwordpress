<?php
class WidgetFilter extends WP_Widget {

    function __construct()
    {
        parent::__construct(
            'filtr-terms',
            'Фильтр для термов'
        );
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }

        $taxonomies = $instance['taxonomies'];

        echo '<form class="textwidget" method="get" action="' . get_page_link( get_option('page_list') ) . '">';
        foreach ($taxonomies as $taxonomyKey => $taxonomy) {
            if (isset($taxonomy)) {
                $oneTaxonomy = get_taxonomies($args = array('name' => $taxonomyKey), $output = 'objects');

                echo '<h4>'. $oneTaxonomy[$taxonomyKey]->label . '</h4>';

                $terms = get_terms(array('taxonomy' => array($oneTaxonomy[$taxonomyKey]->name)));

                foreach ($terms as $term) {
                    $termData = $oneTaxonomy[$taxonomyKey]->name . '-' . $term->term_id;
                    $arrayTaxonomy = $oneTaxonomy[$taxonomyKey]->name;
                    if (isset($_GET[$termData])) {
                        if (in_array($term->term_id, $_GET[$termData])) {
                            echo $this->input_template($termData,
                                                       $arrayTaxonomy,
                                                       $term->term_id,
                                                       $term->name,
                                                       $checkedOption = 'checked');
                        } else {
                            echo $this->input_template($termData,
                                                       $arrayTaxonomy,
                                                       $term->term_id,
                                                       $term->name);
                        }
                    } else {
                        echo $this->input_template($termData,
                                                   $arrayTaxonomy,
                                                   $term->term_id,
                                                   $term->name);
                    }
                }
            }
        }

        echo '<input type="submit" name="submit" value="Filter" />';
        echo '</form>';
        echo '</div>';

        echo $args['after_widget'];

    }

    public function form($instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'text_domain');
        $taxonomies = get_object_taxonomies('music', $output = 'objects');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <?php esc_attr_e('Title:', 'text_domain'); ?>
            </label>
            <input class="widefat"
                   id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                   type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <?php foreach($taxonomies as $taxonomy) { ?>
            <input class="checkbox"
                   type="checkbox" <?php checked( $instance['taxonomies'][$taxonomy->name], 'on'); ?>
                   id="<?php echo esc_attr($this->get_field_id($taxonomy->name)); ?>"
                   name="<?php echo esc_attr($this->get_field_name($taxonomy->name)); ?>" />
            <label for="<?php echo esc_attr($this->get_field_id($taxonomy->name)); ?>"><?php echo esc_html($taxonomy->label) ?></label>
                <br>
            <?php } ?>
        </p>
        <?php
    }

    public function input_template($id, $name, $value, $label, $checkedOption = '')
    {
        $inputMarkup = '<input class="checkbox-music" type="checkbox"' . $checkedOption . ' 
                            data-taxonomy="' . esc_attr__($name) . '" 
                            id="' . esc_attr__($id) . '"
                            name="' . esc_attr__($name) . "[]" . '"
                            value="' . esc_attr__($value) . '"/>
                            <label for="' . esc_attr__($id) . '">' . esc_html__($label) . '</label>
                            <br>';
        return $inputMarkup;
    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';

        $taxonomies = get_object_taxonomies('music', $output = 'objects');
        foreach($taxonomies as $taxonomy) {
            $instance['taxonomies'][$taxonomy->name] = $new_instance[$taxonomy->name];
        }

        return $instance;
    }

}
$my_widget = new WidgetFilter();
