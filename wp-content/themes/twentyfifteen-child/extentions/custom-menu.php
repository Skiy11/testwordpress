<?php
function modify_nav_menu_items($items)
{
    $args = array();
    $output = 'objects';
    $taxonomies = get_taxonomies($args, $output);

    if($taxonomies) {
        foreach ($taxonomies as $my_taxonomy) {
            if ($my_taxonomy->name == 'authorm' || $my_taxonomy->name == 'genre') {
                $items .= "<li class='menu-item  menu-item-has-children'><a href='#'>$my_taxonomy->label</a>";
                $terms = get_terms(array('taxonomy' => array($my_taxonomy->name)));

                $items .= "<ul class='sub-menu toggled-on'>";
                foreach ($terms as $term) {
                    $items .= "<li><a href=\"". get_term_link($term, $taxonomy = '') ."\">$term->name</a></li>";
                }
                $items .= "</ul>";

                $items .= "</li>";
            }
        }
    }

    return $items;
}

add_filter('wp_nav_menu_items', 'modify_nav_menu_items');
