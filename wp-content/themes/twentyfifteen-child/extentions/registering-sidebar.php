<?php
add_action( 'widgets_init', 'music_register_sidebars' );
function music_register_sidebars() {
    register_sidebar(
        array(
            'id'            => 'music',
            'name'          => __( 'Music Sidebar' ),
            'description'   => __( 'A short description of the sidebar.' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
}
