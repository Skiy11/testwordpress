<?php
function add_custom_box()
{
    add_meta_box(
        'wporg_box_id',
        'Характеристики трека',
        'box_html',
        'music'
    );
}
add_action('add_meta_boxes', 'add_custom_box');

function box_html($post)
{
    $music_meta_key = get_post_meta($post->ID, 'music_meta_key', true);
    ?>
    <label for="duration">Длительность трека в секундах:</label>
    <input name="duration" type="text" value="<?php echo $music_meta_key; ?>">
    <?php
}

function save_postdata($post_id)
{
    if (array_key_exists('duration', $_POST)) {
        update_post_meta(
            $post_id,
            'music_meta_key',
            $_POST['duration']
        );
    }
}
add_action('save_post', 'save_postdata');
